import numpy as np
import pandas as pd
from collections import defaultdict
import matplotlib.pyplot as plt
from scipy.cluster import hierarchy
import seaborn as sns
import umap

if __name__ == "__main__":

    sns.set_style("whitegrid")
    mtx = {}
    for j in range(7):
            try:
                mat = np.load("../folds_clean/mat{}_{}.npz".format(j,3))
                mtx[j] = mat
            except Exception as es:
                print(es)
                pass
            
    # test_instances = mtx['test_features']
    # train_instances = mtx['train_features']
    # train_labels = mtx['train_labels']
    # test_labels = mtx['test_labels']
    condition_mapping = {0:"all",1:"2 left",2:"2 right",3: "4 left", 4:"4 right", 5:"2+2 left", 6:"2+2 right"}
    for j in range(7):

        X = mtx[j]['train_features']
        Y = mtx[j]['train_labels']

        
        linked = hierarchy.linkage(X, 'single')
        condition_name = condition_mapping[j]

        dfx2 = pd.DataFrame(X)
        dfx2['label'] = Y        
        sns.clustermap(dfx2[dfx2['label'] ==  1 ])
        plt.title("Condition: {}".format(condition_name))
        plt.ylabel("Patient ID")
        plt.xlabel("Timepoint")
        plt.savefig("figures/healthy_clustermap_{}.png".format(condition_name), dpi = 300)
        plt.clf()

        sns.clustermap(dfx2[dfx2['label'] ==  0 ])
        plt.title("Condition: {}".format(condition_name))
        plt.ylabel("Patient ID")
        plt.xlabel("Timepoint")
        plt.savefig("figures/unhealthy_clustermap_{}.png".format(condition_name), dpi = 300)
        plt.clf()        
        
        hierarchy.dendrogram(linked,
                   orientation='top',
#                   labels=labelList,
                   distance_sort='descending',
                   show_leaf_counts=True)

        plt.title("Condition: {}".format(condition_name))
        plt.savefig("figures/dendrogram_{}.png".format(condition_name), dpi = 300)
        plt.clf()

        X_red = pd.DataFrame(umap.UMAP(n_neighbors=7).fit_transform(X))
        X_red.columns = ['Latent dimension 1','Latent dimension 2']
        X_red['class'] = Y
        X_red['class'] = X_red['class'].astype(str).replace({'1':'Schizoprenic', '0':'Healthy'})
        sns.scatterplot(X_red['Latent dimension 1'], X_red['Latent dimension 2'], hue = X_red['class'], style = X_red['class'], s = 100)
        plt.title("Condition: {}".format(condition_name))
        plt.savefig("figures/umap_{}.png".format(condition_name.replace("+","").replace(" ","")), dpi = 300)
        plt.clf()
        
#       sns.scatterplot(X_red)
