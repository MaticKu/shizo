## basic neural network arcs

## neural network stuff here.

import torch
import tqdm
import torch.nn as nn
from sklearn.preprocessing import OneHotEncoder
from torch.utils.data import DataLoader, Dataset
import logging
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)
import numpy as np

class DatasetLoader_e2e(Dataset):
    
    def __init__(self, features, targets, transform=None):
        self.features = features        
        self.targets = targets
        
    def __len__(self):
        return self.features.shape[0]
    
    def __getitem__(self, index):
        instance = torch.from_numpy(self.features[index, :])
        if self.targets is not None:
            target = torch.as_tensor(self.targets.reshape(-1,1)[index])
        else:
            target = None
        return (instance,target)

def to_one_hot(lbx):
    enc = OneHotEncoder(handle_unknown='ignore')
    return enc.fit_transform(lbx.reshape(-1,1))

def conv_layer(window, ks=3, dilation=1, drop = 0.1):
    return nn.Sequential(
        nn.Conv1d(1, 1, kernel_size=ks, bias=False, dilation=dilation),
        nn.AdaptiveAvgPool1d(window),
        nn.Dropout(drop),
        nn.LeakyReLU(negative_slope=0.1, inplace=True))

class CNN(nn.Module):
    def __init__(self,dropout = 0.1, auto_depth = 1):
        super(CNN, self).__init__()
        mods = []
        for j in range(auto_depth):
            layer = conv_layer(window = 16, drop = dropout)
            mods += (list(layer.children()))
        for j in range(auto_depth):
            layer = conv_layer(window = 8, drop = dropout)
            mods += (list(layer.children()))
        self.conv1 = nn.Sequential(*mods)
        self.fc = nn.Linear(8, 1)
        self.activation = nn.Sigmoid()    
    def forward(self, x):
        x = x.view(x.shape[0], 1,-1)
        out = self.conv1(x)
        out = out.view(x.shape[0], out.size(1) * out.size(2))
        out = self.fc(out)
        out = self.activation(out)
        return out

class DAN(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes, dropout, auto_depth=0):
        super(DAN, self).__init__()
        self.fc1 = nn.Linear(input_size, input_size)
        self.auto_depth = auto_depth
        self.softmax = nn.Softmax(dim=1)
        self.sigma = nn.Sigmoid()
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(dropout)
        self.fc2 = nn.Linear(input_size, hidden_size)
        self.fcx = nn.Linear(hidden_size, hidden_size)
        self.fc3 = nn.Linear(hidden_size, num_classes)
        
    def forward(self, x):
        out = self.fc1(x)
        out = self.relu(out)
        out = self.softmax(out)
        out = out * x
        out = self.fc2(out)
        out = self.dropout(out)

        ## dodatni hidden layer
        # if self.auto_depth > 0:
        #     for j in range(auto_depth):
        #         out = self.fcx(out)
        #         out = self.droput(out)
        
        out = self.fc3(out)
        out = self.sigma(out)
        return out
    
    def get_attention(self, x):
        out = self.fc1(x)
        out = self.softmax(out)
        return out

class ffNN(nn.Module):

    ## pazi na: vedno se mora ujemati stevilo outputov v l1 in inputov v l2, kjer sta l1 in l2 poljubna layerja.
    ## probaj pa: globja mreza. To lako kar v for loopu dodajas
    
    def __init__(self, input_size,num_classes, dropout):
        super(ffNN, self).__init__()
        self.basic = nn.Linear(input_size, num_classes)
        self.first = nn.Linear(num_classes*2, num_classes) ##vmesne dense plasti
        self.sigma = nn.Sigmoid() ## aktivacija v zandji plasti mora biti sigmoida
        self.drop = nn.Dropout(dropout) ## to je regularizacija
        ## 1D convolutional layer
    def forward(self, x):

        ## odkomentiraj spodnje vrstice da poglobis mrezo
        
        out = self.basic(x)
#        out = self.drop(out) 
#        out = self.conv(out)
#        out = self.pool(out)
#        out = self.first(out)
        out = self.sigma(out)
        return out
    
class e2e_DNN:

    def __init__(self,batch_size=1,num_epochs=25,learning_rate=0.0001,stopping_crit=5, architecture="FFNN",hidden_layer_size = 30, dropout = 0.2, file_type=None, auto_depth = 0, condition_filter = None):
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.loss = nn.BCELoss()
        self.auto_depth = auto_depth
        self.dropout = dropout
        self.stopping_crit = stopping_crit
        self.architecture = architecture        
        self.num_epochs = num_epochs
        self.hidden_layer_size = hidden_layer_size
        self.learning_rate = learning_rate

    def fit(self,features,labels,onehot=False):

        if onehot:
            labels = to_one_hot(labels)
        
        train_dataset = DatasetLoader_e2e(features, labels)
        total_step = len(train_dataset)
        stopping_iteration = 0
        loss = 1
        num_classes = 1
        current_loss = 0
        
        if self.architecture == "FFNN":
            self.model = ffNN(features.shape[1], num_classes, self.dropout).to(self.device)
            
        elif self.architecture == "DAN":
            self.model = DAN(features.shape[1], self.hidden_layer_size, 1, self.dropout, self.auto_depth).to(self.device)

        elif self.architecture == "CNN":
            self.model = CNN(dropout = self.dropout, auto_depth = self.auto_depth).to(self.device)
            
        self.model.train()

        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate)
        self.num_params = sum(p.numel() for p in self.model.parameters())
        logging.info("Number of parameters {}".format(self.num_params))
        self.model.train()
        for epoch in range(self.num_epochs):
            if current_loss != loss:
                current_loss = loss
            else:
                stopping_iteration+=1
            if stopping_iteration > self.stopping_crit:
                break
            losses_per_batch = []
            for i, (features,labels) in enumerate(train_dataset):
                features = features.float().to(self.device)
                labels = labels.float().to(self.device)
                outputs = self.model.forward(features)
                loss = self.loss(outputs, labels)
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
                losses_per_batch.append(float(loss))
            mean_loss = np.mean(losses_per_batch)
            
    def predict(self,features):
        test_dataset = DatasetLoader_e2e(features, None)
        predictions = []
        with torch.no_grad():
            for features,_ in test_dataset:                
                features = features.float().to(self.device)
                self.model.eval()
                representation = self.model.forward(features)
                pred = representation.cpu().numpy()[0][0]
                predictions.append(pred)
        return predictions

    def get_attention(self, features):
        test_dataset = DatasetLoader_e2e(features, None)
        predictions = []
        with torch.no_grad():
            for features,_ in test_dataset:
                features = features.float().to(self.device)
                self.model.eval()
                representation = self.model.get_attention(features)
                pred = representation.cpu().numpy()
                predictions.append(pred)
        return predictions
