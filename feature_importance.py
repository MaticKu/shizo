## generate feature importance diagrams
import glob
import scipy.io as sio
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import LeaveOneOut
import tqdm
import logging
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)
from neural import *

def parse_files_to_matrix(fx, file_type):


    if file_type == "raw":

        ## katere vse kombinacije potestiramo:
        ## - 
        
        # 1st dimension is condition: 2 left, 2 right, 4 left, 4 right, 2+2 left, 2+2 right.
        # 2nd dimension is channels: channel labels are in the attached txt file.
        # 3rd dimension is time: time points in milliseconds are in the attached txt file.
        # 4th dimension is trial.

        fmat = sio.loadmat(fx)['data']
        fmx = fmat.shape
        label = fx.split("-")[1][0]
        condition = 0
        total_instances = []
        aggregated_by_trial = np.mean(fmat,axis=3)
        aggregated_by_electrode = np.mean(aggregated_by_trial,axis=1)
        aggregated_by_condition = np.mean(aggregated_by_electrode,axis=0)
        if label == "P":
            label = 1
        else:
            label = 0
        return (aggregated_by_condition,label)
    
    elif file_type == "cleaned":
        fmat = sio.loadmat(fx)['ipower'].T
        fmx = fmat.shape
        label = fx.split("-")[1][0]
        condition = 0
        total_instances = []
        agg = fmat
        if label == "P":
            label = 1
        else:
            label = 0
        return (agg, label)



if __name__ == "__main__":


    
    
    pass
