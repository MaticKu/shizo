### parse EEG matrices to suitable format.
import glob
import scipy.io as sio
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.model_selection import LeaveOneOut
import tqdm
import logging
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)
from neural import *

def parse_files_to_matrix(fx, file_type, condition_filter = 0):


    if file_type == "raw":

        ## katere vse kombinacije potestiramo:
        ## - 
        
        # 1st dimension is condition: 2 left, 2 right, 4 left, 4 right, 2+2 left, 2+2 right.
        # 2nd dimension is channels: channel labels are in the attached txt file.
        # 3rd dimension is time: time points in milliseconds are in the attached txt file.
        # 4th dimension is trial.

        fmat = sio.loadmat(fx)['data']
        fmx = fmat.shape
        label = fx.split("-")[1][0]
        condition = 0
        total_instances = []
        aggregated_by_trial = np.mean(fmat,axis=3)
        aggregated_by_electrode = np.mean(aggregated_by_trial,axis=1)
        if condition_filter == 0: ## use all conditions
            aggregated_by_condition = np.mean(aggregated_by_electrode,axis=0)

        elif condition_filter == 7:
            aggregated_by_condition = np.mean(aggregated_by_electrode[3:,:], axis=0)
        
        else: ## use only a single condition
            aggregated_by_condition = aggregated_by_electrode[condition_filter-1,:]
            
        if label == "P":
            label = 1
            
        else:
            label = 0
            
        return (aggregated_by_condition,label)
    
    elif file_type == "cleaned":
        fmat = sio.loadmat(fx)['ipower'].T
        fmx = fmat.shape
        label = fx.split("-")[1][0]
        condition = 0
        total_instances = []
        agg = fmat
        if label == "P":
            label = 1
        else:
            label = 0
        return (agg, label)

def parse_clean_files_to_matrix(fx):
    
    fmat = sio.loadmat(fx)['ipower'].T
    fmx = fmat.shape
    label = fx.split("-")[1][0]

    if label == "P":
        label = 1
    else:
        label = 0
    return (fmx, label)

def generate_a_model(model_type = None, kwargs = {}):
#    batch_size, num_epochs, learning_rate, hidden_layer_size = hidden_layer_size, architecture = architecture, dropout = dropout
    
    if model_type == "DNN" or model_type == "FFNN" or model_type == "CNN":
        del kwargs['C']
        return e2e_DNN(**kwargs)
    
    elif model_type == "LR":
        return LogisticRegression(C = kwargs['C'])
    
    elif model_type == "SVM_poly":
        return SVC(C = kwargs['C'], kernel = "poly")

    elif model_type == "SVM_linear":
        return SVC(C = kwargs['C'], kernel = "linear")

    elif model_type == "SVM_rbf":
        return SVC(C = kwargs['C'], kernel = "rbf")

    elif model_type == "KNN":
        return KNeighborsClassifier(kwargs['C'])
    
    elif model_type == "RF":
        return RandomForestClassifier(kwargs['C'])

def validate_a_model(clf, kwargs,all_files, type_of_data, get_attention = False, get_coefficients = False):

    logging.info(type_of_data)
    condition_mapping = {0:"all",1:"2 left",2:"2 right",3: "4 left", 4:"4 right", 5:"2+2 left", 6:"2+2 right",7:"without 2s"}
    
    TP = 0
    FP = 0
    TN = 0
    FN = 0
    fold = 0
    attention_vectors = []
    if len(all_files) == 0:
        all_files = ["placeholder"] * 26
    for tindex,te in loo.split(all_files):
        train_instances = []
        train_labels = []
        fold+=1
        
        try:
            additional_info = str(kwargs['condition_filter']) ## conditions
            fold_data = np.load('../'+type_of_data+'/mat{}_{}.npz'.format(additional_info,fold))
            test_instances = fold_data['test_features']
            train_instances = fold_data['train_features']
            train_labels = fold_data['train_labels']
            test_labels = fold_data['test_labels']
            save = False
            
        except Exception as es:
            
            condition_filter = kwargs['condition_filter']
            logging.info("Condition filter is now {}".format(condition_filter))
            save = True
            logging.info(es)            
            test_instances,test_labels = parse_files_to_matrix(all_files[te[0]],"raw", condition_filter)
            for el in tqdm.tqdm(tindex):

                features, label = parse_files_to_matrix(all_files[el], "raw", condition_filter)
                train_instances.append(features)
                train_labels.append(label)
    
        train_instances = np.matrix(train_instances)
        train_labels = np.array(train_labels)
        logging.info("Train shape {}".format(train_instances.shape))
        clf.fit(train_instances,train_labels)
        test_instances = np.matrix(test_instances)
        preds = clf.predict(test_instances)
        if save:
            np.savez('../{}/mat{}_{}.npz'.format(type_of_data,str(kwargs['condition_filter']),fold), train_features=train_instances,train_labels=train_labels,test_features = test_instances, test_labels=test_labels)
            
        if preds[0] > 0.5:            
            preds_real = 1            
        else:
            preds_real = 0
        logging.info("Real {}, predicted {}, prob {}".format(test_labels,preds_real,preds))
        if preds_real == 1 and test_labels == 1:
            TP +=1
            if get_attention:
                attention_vectors.append(clf.get_attention(test_instances)[0].flatten())
            if get_coefficients:
                stdx = np.std(test_instances)
                attention_vectors.append(stdx * clf.coef_)
        elif preds_real == 1 and test_labels == 0:
            FP +=1
        elif preds_real == 0 and test_labels == 0:
            TN+=1
            if get_attention:
                attention_vectors.append(clf.get_attention(test_instances)[0].flatten())
            if get_coefficients:
                stdx = np.std(test_instances)
                attention_vectors.append(stdx * clf.coef_)
        else:
            FN+=1
        if get_attention:
            logging.info("Collected {} attention vectors.".format(len(attention_vectors)))
        logging.info("TP: {}, FP: {}, TN: {} FN: {}".format(TP,FP,TN,FN))

    accuracy = (TP+TN)/(TP+TN+FP+FN)
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    
    try:
        F1 = 2* (precision*recall)/(precision+recall)
        
    except:
        F1 = 0

    if get_attention:
        attention_vectors = np.mean(np.matrix(attention_vectors),axis=0)
        np.save("attention_vectors/attention_vectors",attention_vectors)

    if get_coefficients:
        v = np.concatenate(attention_vectors, axis = 0)
        attention_vectors = np.mean(v,axis=0)
        np.save("attention_vectors/regression_coefficients",attention_vectors)

    if not "C" in kwargs:
        kwargs['C'] = None
        
    print("\t".join(str(x) for x in ["FINAL_RESULT", accuracy, precision, recall, F1, kwargs['batch_size'], kwargs['num_epochs'], kwargs['learning_rate'], kwargs['architecture'], kwargs['file_type'], kwargs['dropout'], type_of_data, kwargs['auto_depth'], kwargs['hidden_layer_size'], condition_mapping[kwargs['condition_filter']], kwargs['C']]))


def load_nas_file(nas_tsv):

    pdx = pd.read_csv(nas_tsv, sep="\t")
    columns = ["tag","Accuracy","Precision","Recall","F1","batch_size","num_epochs","learning_rate","architecture","file_type","dropout","filetype","auto_depth","hidden_layer_size","condition"]
    pdx.columns = columns
    pdx = pdx[pdx['architecture'] == "DAN"].sort_values(by=["F1"], ascending=False)
    pdx['dropout'] = pdx['dropout'].astype(float)
    pdx['hidden_layer_size'] = pdx['hidden_layer_size'].astype(int)
    pdx['num_epochs'] = pdx['num_epochs'].astype(int)
    pdx['learning_rate'] = pdx['learning_rate'].astype(float)
    pdx['auto_depth'] = pdx['auto_depth'].astype(int)
    kwargs = pdx.iloc[0].to_dict()
    
    del kwargs['tag']
    del kwargs['Accuracy']
    del kwargs['F1']
    del kwargs['Precision']
    del kwargs['Recall']
    del kwargs['filetype']
    del kwargs['condition']    
    kwargs['condition_filter'] = 6
    return kwargs
    
    
if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_type",default="raw")
    parser.add_argument("--experiment",default="NAS")
    parser.add_argument("--nas_results",default="results/clean_dump7.txt")
    args= parser.parse_args()
    file_type = args.data_type
    if args.experiment == "explain":

        files_path = "../../Documents/EEG/ML/*"    
        type_of_data = "folds_aggregated_all_conditions"
        file_type = "condition"
        all_files = glob.glob(files_path)
        loo = LeaveOneOut()
        ## 5 5 0.1 0.01

        batch_size = 1
        num_epochs = 10
        learning_rate = 0.0005
        hidden_layer_size = 64
        architecture = "LR"
        dropout = 0.1
        auto_depth = 0
        condition = 0
        for j in [7]:
            kwargs = {"batch_size":batch_size, "num_epochs" : num_epochs, "learning_rate" : learning_rate, "hidden_layer_size" : hidden_layer_size, "architecture" : architecture, "dropout" : dropout, "file_type":file_type, "auto_depth":auto_depth, "condition_filter":j}

            clf = generate_a_model(architecture, kwargs)
            validate_a_model(clf,kwargs,all_files, type_of_data)

    elif args.experiment == "explain_time":
        kwargs = load_nas_file(args.nas_results)
        print(kwargs)
        clf = generate_a_model("DNN", kwargs)
        loo = LeaveOneOut()
        files_path = "../../Documents/EEG/ML_laplacian/*"    
        type_of_data = "folds_clean"
        all_files = glob.glob(files_path)
        validate_a_model(clf,kwargs,all_files, type_of_data, get_attention = True)    

    elif args.experiment == "NAS":

        if file_type == "raw":
            files_path = "../../Documents/EEG/ML/*"
            type_of_data = "folds_aggregated_all"

        elif file_type == "laplacian":
            files_path = "../../Documents/EEG/ML_laplacian/*"    
            type_of_data = "folds_clean"

        elif file_type == "laplacian64":
            files_path = "../../Documents/EEG/ML_laplacian_64/*"    
            type_of_data = "folds_clean_64"

        all_files = glob.glob(files_path)
        loo = LeaveOneOut()

        ## baseline LR
        batch_size = None
        num_epochs = None
        learning_rate = None
        hidden_layer_size = None
        architecture = None
        condition_filter = 0
        dropout = None
        
        for condition_filter in [7]:

            for cparam in [1,2,4,8,16,31,64,128]:
                kwargs = {"batch_size":batch_size, "num_epochs" : num_epochs, "learning_rate" : learning_rate, "hidden_layer_size" : hidden_layer_size, "architecture" : "LR", "dropout" : dropout, "file_type":file_type,"auto_depth":None, "condition_filter":condition_filter, "C" : cparam}

                clf = generate_a_model("LR",kwargs)
                if condition_filter == 7:
                    validate_a_model(clf, kwargs, all_files, type_of_data, get_coefficients = True)
                else:
                    validate_a_model(clf, kwargs, all_files, type_of_data)

                kwargs['architecture'] = "SVM_rbf"
                clf = generate_a_model("SVM_rbf",kwargs)
                validate_a_model(clf, kwargs, all_files, type_of_data)

                kwargs['architecture'] = "SVM_linear"
                clf = generate_a_model("SVM_linear",kwargs)
                validate_a_model(clf, kwargs, all_files, type_of_data)

                kwargs['architecture'] = "SVM_poly"
                clf = generate_a_model("SVM_poly",kwargs)
                validate_a_model(clf, kwargs, all_files, type_of_data)

            for kn_param in [2,3,4,5,6,7,8,9,10]:

                kwargs = {"batch_size":batch_size, "num_epochs" : num_epochs, "learning_rate" : learning_rate, "hidden_layer_size" : hidden_layer_size, "architecture" : "LR", "dropout" : dropout, "file_type":file_type,"auto_depth":None, "condition_filter":condition_filter, "C" : kn_param}
                kwargs['architecture'] = "KNN"
                clf = generate_a_model("KNN",kwargs)
                validate_a_model(clf, kwargs, all_files, type_of_data)

            for ntree in [50,100]:
                kwargs = {"batch_size":batch_size, "num_epochs" : num_epochs, "learning_rate" : learning_rate, "hidden_layer_size" : hidden_layer_size, "architecture" : "LR", "dropout" : dropout, "file_type":file_type,"auto_depth":None, "condition_filter":condition_filter, "C" : ntree}
                kwargs['architecture'] = "RF"
                clf = generate_a_model("RF",kwargs)
                validate_a_model(clf, kwargs, all_files, type_of_data)

        for condition_filter in [7]:
            for architecture in ["CNN"]:
                for auto_depth in [2,4,8,12]:
                    for dropout in [0.01,0.05,0.2,0.5]:
                        for hidden_layer_size in [16,32,64,128]:
                            for batch_size in [1]:
                                for num_epochs in [2, 4, 8, 16, 32]:
                                    for learning_rate in [0.0001,0.001]:
                                        kwargs = {"batch_size":batch_size, "num_epochs" : num_epochs, "learning_rate" : learning_rate, "hidden_layer_size" : hidden_layer_size, "architecture" : architecture, "dropout" : dropout, "file_type":file_type, "auto_depth":auto_depth,"condition_filter": condition_filter, "C" : 1}

                                        clf = generate_a_model("DNN", kwargs)
                                        validate_a_model(clf,kwargs,all_files, type_of_data)
        
        for condition_filter in [7]:
            for architecture in ["DAN","FFNN"]:
                for auto_depth in [0]:
                    for dropout in [0.01,0.05,0.2,0.5]:
                        for hidden_layer_size in [16,32,64,128]:
                            for batch_size in [1]:
                                for num_epochs in [2, 4, 8, 16, 32]:
                                    for learning_rate in [0.0001,0.001]:
                                        kwargs = {"batch_size":batch_size, "num_epochs" : num_epochs, "learning_rate" : learning_rate, "hidden_layer_size" : hidden_layer_size, "architecture" : architecture, "dropout" : dropout, "file_type":file_type, "auto_depth":auto_depth,"condition_filter": condition_filter, "C" : 1}

                                        clf = generate_a_model("DNN", kwargs)
                                        validate_a_model(clf,kwargs,all_files, type_of_data)
